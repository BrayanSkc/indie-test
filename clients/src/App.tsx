import React, { useEffect } from "react";
import Task from "./screens/task/index";
import Login from "./screens/login/index";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  RouteComponentProps,
  Redirect,
} from "react-router-dom";

import { DASHBOARD, LOGIN, SIGN_UP } from "./routes";
import SignUp from "./screens/signUp/index";
import { getUserLogin } from "./services";
import Header from "./components/header";

const AppContent: React.FC<RouteComponentProps> = ({ history }) => {
  useEffect(() => {
    const user = getUserLogin();
    if (!user) {
      history.push(LOGIN);
    }
  }, [history]);

  return (
    <Switch>
      <Route exact path={DASHBOARD} component={Task}></Route>
      <Route render={() => <Redirect to={DASHBOARD} />}></Route>
    </Switch>
  );
};

const App = () => {
  return (
    <Router>
      <Header />
      <Switch>
        <Route exact path={LOGIN} component={Login}></Route>
        <Route exact path={SIGN_UP} component={SignUp}></Route>
        <Route component={AppContent}></Route>
      </Switch>
    </Router>
  );
};

export default App;
