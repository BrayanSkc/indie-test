import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { SIGN_UP, LOGIN } from "../../routes";
import { getUserLogin } from "../../services";
import "./style.css";
export interface HeaderProps {}
const logo =
  "https://indielevelstudio.com/wp-content/uploads/2020/04/logoils-1.png";

const user = getUserLogin();

const Header: React.FC<HeaderProps> = () => {
  const [active, setActive] = useState("1");

  const exit = () => {
    localStorage.removeItem("user_Task");
    window.location.reload();
  };

  const getClass = (id: string) =>
    id === active ? "options-menu-bar active-bar" : "options-menu-bar";
  return (
    <div className="content-header-bar">
      <div className="left-img-header">
        <img src={logo} className="img-logo-header" alt="icono" />
      </div>
      <div className="right-header-bar">
        {user ? (
          <ul className="li-options-menu">
            <span className="options-menu-bar" onClick={exit}>
              Salir
            </span>
          </ul>
        ) : (
          <ul className="li-options-menu">
            <Link
              to={LOGIN}
              className={getClass("1")}
              onClick={() => setActive("1")}
            >
              Login
            </Link>
            <Link
              to={SIGN_UP}
              className={getClass("2")}
              onClick={() => setActive("2")}
            >
              Sign Up
            </Link>
          </ul>
        )}
      </div>
    </div>
  );
};

export default Header;
