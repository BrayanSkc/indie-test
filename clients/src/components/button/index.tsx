import React, { Children } from "react";
import "./style.css";
export interface ButtonProps {
  children: any;
  className?: string;
  onClick: () => void;
}

const Button: React.FC<ButtonProps> = ({
  children,
  className = "",
  onClick,
}) => {
  return (
    <button
      className={"btn-principal-color " + className}
      onClick={onClick}
    >
      {children}
    </button>
  );
};

export default Button;
