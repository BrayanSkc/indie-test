import React, { useMemo, useState, useEffect } from "react";
import { FcCheckmark } from "react-icons/fc";
import { AiFillDelete, AiOutlineMenu, AiOutlineClose } from "react-icons/ai";
import Button from "../../components/button";
import "./style.css";
import { generateId } from "../../helpers/index";
import {
  getTasks,
  createTask,
  deleteTask,
  updateTask,
} from "../../services/index";

export interface TaskProps {}
interface taskInterface {
  taskId: string;
  status: boolean;
  name: string;
}

const Task: React.FC<TaskProps> = () => {
  const [newTask, setNewTask] = useState("");
  const [filter, setFilter] = useState("");
  const [listTask, setListTask] = useState<taskInterface[]>([]);
  const [reload, setReload] = useState(false);

  /**
   * Obtiene las tareas previamente creadas
   *
   */

  useEffect(() => {
    try {
      const getTask = async () => {
        const resolveTask = await getTasks();
        const data: taskInterface[] = resolveTask.map((item: any) => {
          return {
            taskId: item._id,
            status: item.status,
            name: item.name,
          };
        });
        setListTask(data);
      };
      getTask();
    } catch (err) {
      console.log(err);
      alert('error al cargar tareas')
    }
  }, [reload]);

  const addNewTask = async () => {
    try {
      setListTask([
        ...listTask,
        { taskId: generateId(), status: false, name: newTask },
      ]);
      await createTask({ taskId: generateId(), status: false, name: newTask });
      setNewTask("");
    } catch (err) {
      alert("ocurrio un error creando la tarea");
    }
  };
  /**
   *  función que elimina una tarea de la lista de tareas
   *
   */
  const deleteTaskPanel = async (idTask: string) => {
    try {
      await deleteTask(idTask);
      setReload(!reload);
    } catch (err) {
      alert("Error al eliminar la tarea");
    }
  };

  /**
   * Esta función muestra la lista de tareas segun el filtro (si es que existe)
   */
  const getList = useMemo(
    () =>
      filter === "New"
        ? listTask.filter((item: taskInterface) => !item.status)
        : filter === "Closed"
        ? listTask.filter((item: taskInterface) => item.status)
        : listTask,
    [filter, listTask]
  );

  /**
   *  función que actualiza el estado de las  (completadas y no-completadas)
   *
   */

  const changeStatus = async (idTask: string, status: boolean) => {
    try {
      await updateTask(idTask, { status });
      setReload(!reload);
    } catch (err) {
      alert("error al actualizar el estado de la tarea");
    }
  };

  /**
   *  función que devuelve un estilo segun estado de la tarea
   *
   */
  const sideStyle = (status: boolean) =>
    status ? "text-task through" : "text-task ";
  const getCount = useMemo(
    () => listTask.filter((item: taskInterface) => item.status).length,
    [listTask]
  );

  return (
    <div className="container-task-background">
      <div className="box-task-content">
        <div className="header-box-task">
          <div className="options-header-box">
            <span className="text-title-principal">Lista de tareas</span>
            <div className="filter-options-header">
              <Button onClick={() => setFilter("ALL")}>
                <AiOutlineMenu />
                Todas
              </Button>
              <Button onClick={() => setFilter("New")}>
                <AiOutlineClose className="red" />
                Tareas
              </Button>
              <Button onClick={() => setFilter("Closed")}>
                <FcCheckmark />
                Completadas
              </Button>
            </div>
          </div>
        </div>

        <ul className="ul-task-row">
          {getList.length > 0 ? (
            getList.map((item: taskInterface, index: number) => (
              <li className="li-task-row" key={item.taskId}>
                <div className="left-name-task">
                  <input
                    type="checkbox"
                    value={item.taskId}
                    checked={item.status}
                    onChange={({ target }) =>
                      changeStatus(item.taskId, target.checked)
                    }
                  />
                  <span className={sideStyle(item.status)}>{item.name} </span>
                </div>

                <AiFillDelete
                  className="red"
                  onClick={() => deleteTaskPanel(item.taskId)}
                />
              </li>
            ))
          ) : (
            <span className="text-empty-state">Sin tareas que mostrar</span>
          )}
        </ul>

        <div className="footer-box-task">
          <div className="input-group-footer">
            <input
              className="input-value-box"
              placeholder="Agregar tarea"
              type="text"
              value={newTask}
              onChange={({ target }) => setNewTask(target.value)}
            />
            <Button onClick={addNewTask} className="add" children="+ Agregar" />
          </div>

          <div className="counter-task-footer">
            <span className="text-title-principal fs-12">
              Tareas: {getCount} /{listTask.length}
            </span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Task;
