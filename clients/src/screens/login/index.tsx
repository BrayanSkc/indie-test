import React, { useState, useCallback } from "react";
import Button from "../../components/button";
import "../task/style.css";
import "./style.css";
import { loginUser, setUserLogin } from "../../services/index";
import { DASHBOARD } from "../../routes";
import { RouteComponentProps } from "react-router-dom";

export interface LoginProps {}

interface loginInterface {
  email: string;
  password: string;
}

const Login: React.FC<RouteComponentProps> = ({ history }) => {
  const [userInfo, setUserInfo] = useState<loginInterface>({
    email: "",
    password: "",
  });

  const handleChangeInput = useCallback(
    (key: keyof loginInterface) => ({ target: { value } }: any) => {
      setUserInfo({
        ...userInfo,
        [key]: value,
      });
    },
    [userInfo]
  );

  const submitLogin = async () => {
    try {
      const resolve = await loginUser(userInfo);
      if (resolve.status === 201) {
        setUserLogin(resolve.data);
        history.push(DASHBOARD);
        window.location.reload();
      } else {
        alert("por favor rectifique los datos");
      }
    } catch (err) {
      alert("ocurrio un error ingresando, por favor revise los datos");
    }
  };

  return (
    <div className="container-task-background">
      <div className="box-task-content w-30">
        <div className="header-box-task">
          <div className="options-header-box">
            <span className="text-title-principal">Iniciar Sesión</span>
          </div>
        </div>
        <div className="body-form-login">
          <label htmlFor="email" children="Email :" />
          <input
            type="text"
            className="input-login-value"
            id="email"
            value={userInfo.email}
            onChange={handleChangeInput("email")}
          />
          <label htmlFor="pass" children="Contraseña: " />
          <input
            type="password"
            className="input-login-value"
            id="pass"
            value={userInfo.password}
            onChange={handleChangeInput("password")}
          />
          <Button className="btn-login" onClick={submitLogin}>
            Ingresar
          </Button>
        </div>
      </div>
    </div>
  );
};

export default Login;
