import React, { useState, useCallback } from "react";
import { RouteComponentProps } from "react-router-dom";
import Button from "../../components/button";
import { LOGIN } from "../../routes";
import { createUser } from "../../services";
import "./style.css";
import { validEmail, validText, generateId } from "../../helpers/index";

export interface SignUpProps {}
interface userInfoInterface {
  email: string;
  name: string;
  password: string;
  userId: string;
}

const SignUp: React.FC<RouteComponentProps> = ({ history }) => {
  const [userInfo, setUserInfo] = useState<userInfoInterface>({
    email: "",
    name: "",
    password: "",
    userId: "",
  });

  const handleChangeInput = useCallback(
    (key: keyof userInfoInterface) => ({ target: { value } }: any) => {
      setUserInfo({
        ...userInfo,
        [key]: value,
      });
    },
    [userInfo]
  );

  const submitInfo = async () => {
    try {
      if (!validText(userInfo.name)) {
        alert("Ingrese un nombre valido");
        return;
      }
      if (!validText(userInfo.password)) {
        alert("Ingrese una contraseña valida");
        return;
      }
      if (!validEmail(userInfo.email)) {
        alert("Ingrese un email valido");
        return;
      }

      userInfo.userId = generateId();

      await createUser(userInfo);
      history.push(LOGIN);
    } catch (err) {
      alert("ocurrio un error ingresando, por favor revise los datos");
    }
  };

  return (
    <div className="container-task-background">
      <div className="box-task-content w-30 hg-70">
        <div className="header-box-task">
          <div className="options-header-box">
            <span className="text-title-principal">Registrarse</span>
          </div>
        </div>
        <div className="body-form-login">
          <label htmlFor="name" children="Nombre :" />
          <input
            type="text"
            className="input-login-value"
            id="name"
            value={userInfo.name}
            onChange={handleChangeInput("name")}
          />
          <label htmlFor="email" children="Email :" />
          <input
            type="text"
            className="input-login-value"
            id="email"
            value={userInfo.email}
            onChange={handleChangeInput("email")}
          />
          <label htmlFor="pass" children="Contraseña: " />
          <input
            type="password"
            className="input-login-value"
            id="pass"
            value={userInfo.password}
            onChange={handleChangeInput("password")}
          />
          <Button className="btn-login" onClick={submitInfo}>
            Registarse
          </Button>
        </div>
      </div>
    </div>
  );
};

export default SignUp;
