const API_URL = "http://localhost:4000/";
/**
 * Metodo que hace la peticion a el servidor
 * @param url  Url que completa el servidor, puede incluir paramtros
 * @param method  metodo con el que se realiza la petición
 * @param params  body de la peticion
 */
const apiFetch = async (
  url?: string,
  method: "GET" | "POST" | "DELETE" | "PUT" = "GET",
  params?: object
) => {
  try {
    const dataResult = await fetch(API_URL + url, {
      headers: {
        "Content-Type": "application/json",
      },
      method,
      body: JSON.stringify(params),
    });
    if (!dataResult.ok) {
      const { message } = await dataResult.json();
      throw message;
    }
    return await dataResult.json();
  } catch (error) {
    throw error;
  }
};
/**
 *  Funcion que trae los registros de DB
 */
export const getTasks = async () => {
  const _tasks: any[] = await apiFetch("api/task");
  return _tasks;
};

/**
 *  Funcion que crea un registro en la DB
 */

export const createTask = async (data: any) => {
  const resp = await apiFetch("api/task", "POST", data);
  return resp;
};
/**
 *  Funcion que elimina un registro de la DB
 */

export const deleteTask = async (id: string) => {
  try {
    const resolve = await apiFetch(`api/task/${id}`, "DELETE");
    return resolve;
  } catch (err) {
    console.log(err);
  }
};
/**
 *  Funcion que actualiza un regstro en la DB
 */

export const updateTask = async (id: string, data: any) => {
  const resp = await apiFetch(`api/task/${id}`, "PUT", data);
  return resp;
};

export const getUserLogin = () => {
  return localStorage.getItem("user_Task");
};
export const setUserLogin = (value: string) => {
  return localStorage.setItem("user_Task", value);
};

export const loginUser = async (data: any) => {
  const resp = await apiFetch(`api/user/login`, "POST", data);
  return resp;
};
export const createUser = async (data: any) => {
  const resp = await apiFetch(`api/user`, "POST", data);
  return resp;
};
