import { createBrowserHistory } from "history";
export const history = createBrowserHistory();

export const generateId = () => {
  const date = new Date();
  return date.getTime().toString();
};

export const validEmail = (email: string) => {
  if (!/^([a-zA-Z0-9_\-.]+)@([a-zA-Z0-9]+)\.([a-zA-Z]{2,5})$/.test(email)) {
    return false;
  }
  return true;
};

export const validText = (text: string) => {
  if (text.trim() == "") {
    return false;
  }
  return true;
};
