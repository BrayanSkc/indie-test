# indie-test

//Instrucciones

* Git clone al proyecto para tener acceso a este localmente, esta acción dejara una carpeta llamada "indie-test" (sin comillas) .
* Al ingresar a la carpeta iremos a la carpeta "server" ahi instalaremos las dependencia con "yarn install o yarn install --save (Opcional)" 
* Una vez terminado esto, seria realizar un "yarn run dev" para ejecutar el servicio en NodeJs con mongo, esto estara corriendo sobre el puerto 4000
* Una vez corriendo el server, iremos a la carpeta clients y realizamos el mismo proceso de instalacion de dependencia (paso 2)
* Una vez terminado esto, seria realizar un "yarn run start" con esto deberia de arrancar nuestra aplicación en el puerto 3000  


Pd: es importante tener corriendo el servicio de mongo de manera local.
Para esto ejecutar desde la terminal "mongod", si se trabaja sobre windows puede ser necesario configurar la variable de entorno (O ejecutar desde el powershell)

//Author: Brayan Escalona,  EndingTime: 06/12/20  - 16:50