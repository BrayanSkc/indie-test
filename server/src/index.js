//** Modulo utilizadopara arrancar nuestro archivo App.Js ** */

const app = require("./app");
require("./database");
async function main() {
  await app.listen(app.get('port'));
  console.log("Puerto desde 4000");
}
main();
