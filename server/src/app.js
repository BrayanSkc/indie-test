/**
 * desde este documento manejaremos el flujo de nuestro servicio, definimos puertos, habilitamos cors para interactuar con nuestro service
 * y definimos ruta a la cual haremos peticion
 */

const express = require("express");
const app = express();
const cors = require("cors");
const { json } = require("express");
//port
app.set("port", 4000);

//Access
app.use(cors());
app.use(express.json());
//routes
app.use("/api/task", require("./routes/task"));
 app.use("/api/user", require("./routes/user"));


module.exports = app;
