const { Schema, model } = require("mongoose");
const { type } = require("os");

const taskSchema = new Schema({
  taskId: {
    type: String,
  },
  name: {
    type: String,
  },
  status: {
    type: Boolean,
  },
});
module.exports = model("task", taskSchema);
