const { Schema, model } = require("mongoose");

const usersSchema = new Schema({
  userId: {
    type: String,
  },
  name: {
    type: String,
  },
  email: {
    type: String,
  },
  password: {
    type: String,
  },
});
module.exports = model("users", usersSchema);
