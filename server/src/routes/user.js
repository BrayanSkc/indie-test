const { Router } = require("express");
const router = Router();
const { getUser, createUser, loginUser } = require("../controllers/users.controllers");
/**
 * definimos nuestras rutas a trabajar y/o consultar, con estos los métodos que aplican para estos mismos
 */

router.route("/login").post(loginUser);
router.route("/").post(createUser).get(getUser);

module.exports = router;
