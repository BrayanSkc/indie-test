const { Router } = require("express");
const router = Router();
const {
  getTask,
  createTask,
  updateTask,
  deleteTask,
} = require("../controllers/task.controllers");
/**
 * definimos nuestras rutas a trabajar y/o consultar, con estos los métodos que aplican para estos mismos
 */

router.route("/").get(getTask).post(createTask);
router.route("/:id").delete(deleteTask).put(updateTask);




module.exports = router;
