const userCtrl = {};
const Users = require("../models/users");
const { Types } = require("mongoose");
/**
 * service que trae de DB todos los registros
 * @param {*} req  no esta en uso
 * @param {*} res procede a dar la respuesta a la peticion
 */

userCtrl.getUser = async (req, res) => {
  try {
    const users = await Users.find();
    res.json(users);
  } catch (err) {
    res.json({ status: 403, message: "Ha ocurrido un error" });
  }
};
/**
 * Crea una tarea nueva
 * @param {body} req  // Datos a recibir para la creación de la tarea, estructura definida en el esquema
 * @param {*} res  //se utiliza para devolver la erspuesta de la consulta
 */

userCtrl.createUser = async (req, res) => {
  try {
    const { userId, name, email, password } = req.body;
    const newUser = new Users({
      userId,
      name,
      email,
      password,
    });
    await newUser.save();
    res.json({ status: 201, message: "se ha creado el usuario correctamente" });
  } catch (err) {
    res.json({ status: 403, message: "Ha ocurrido un error" });
  }
};

userCtrl.loginUser = async (req, res) => {
  try {
    const { email, password } = req.body;
    const user = await Users.findOne({ email, password }).exec();
    if (!user) {
      throw new exception;
    }
    res.json({ status: 201, data: user.name });
  } catch (err) {
    res.json({ status: 403, message: "Ha ocurrido un error" });
  }
};

module.exports = userCtrl;
