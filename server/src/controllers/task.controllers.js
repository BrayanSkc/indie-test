const taskCtrl = {};
const Task = require("../models/task");
const { Types } = require("mongoose");
/**
 * service que trae de DB todos los registros
 * @param {*} req  no esta en uso
 * @param {*} res procede a dar la respuesta a la peticion
 */

taskCtrl.getTask = async (req, res) => {
  try {
    const task = await Task.find();
    res.json(task);
  } catch (err) {
    res.json({ status: 403, message: "Ha ocurrido un error" });
  }
};
/**
 * Crea una tarea nueva
 * @param {body} req  // Datos a recibir para la creación de la tarea, estructura definida en el esquema
 * @param {*} res  //se utiliza para devolver la erspuesta de la consulta
 */

taskCtrl.createTask = async (req, res) => {
  try {
    const { taskId, name, status } = req.body;
    const newTask = new Task({
      taskId,
      name,
      status,
    });
    await newTask.save();
    res.json({ status: 201, message: "se ha creado la nota correctamente" });
  } catch (err) {
    res.json({ status: 403, message: "Ha ocurrido un error" });
  }
};
/**
 * Elimina una tarea de Db
 * @param {params.id} req  recibimos el id de la tarea a eliminar
 * @param {*} res  //se utiliza para devolver la erspuesta de la consulta
 */
taskCtrl.deleteTask = async (req, res) => {
  try {
    const id = req.params.id;
    await Task.findByIdAndDelete(id);
    res.json({ status: 201, message: "tarea eliminada" });
  } catch (err) {
    res.json({ status: 403, message: "Ha ocurrido un error" });
  }
};
/**
 * Actualiza el estado de la peticion
 * @param {id, body} req  //Recibe como parametro el id a editar y como cuerpo de peticion, el estado
 * @param {*} res //se utiliza para devolver la erspuesta de la consulta
 */

taskCtrl.updateTask = async (req, res) => {
  try {
    const id = req.params.id;
    const { status } = req.body;
    const task = await Task.updateOne({ _id: Types.ObjectId(id) }, { status });
    res.json({ status: 201, message: "tarea actualizada" });
  } catch (err) {
    res.json({ status: 403, message: "Ha ocurrido un error" });
  }
};

module.exports = taskCtrl;
