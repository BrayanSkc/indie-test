
/**
 * conexion con nuestra DB de mongo
 */
const mongoose = require("mongoose");
const URI_DB = "mongodb://localhost/test"

mongoose.connect(URI_DB, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true

});

const conection = mongoose.connection;

conection.once("open", () => {
  console.log("Connected");
});
